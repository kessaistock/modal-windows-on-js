let fruits = [
    {id: 1, title: 'Apples', price: 20, img: 'https://m.dom-eda.com/uploads/images/catalog/item/86df51de21/c25c94fe96_1000.jpg'},
    {id: 2, title: 'Oranges', price: 30, img: 'https://m.dom-eda.com/uploads/images/catalog/item/dfc9a3e974/3cbf3bd41c_1000.jpg'},
    {id: 3, title: 'Bananas', price: 40, img: 'https://m.dom-eda.com/uploads/images/catalog/item/7e6fd19d46/0c87fefc9a_1000.jpg'},
]

const toHTML = fruit => `
    <div class="col">
        <div class="card">
            <img class="card-img-top" style="height: 300px;" src="${fruit.img}">
            <div class="card-body">
                <h5 class="card-title">${fruit.title}</h5>
                <a href="#" class="btn btn-primary" data-btn="price" data-id="${fruit.id}">Show price</a>
                <a href="#" class="btn btn-danger" data-btn="remove" data-id="${fruit.id}">Delete</a>
            </div>
        </div>
    </div>
`

function render () {
    const html = fruits.map(fruit => toHTML(fruit)).join('');
    document.getElementById('fruits').innerHTML = html;
}

render();

const priceModal = $.modal({
    title: 'Price for the goods',
    closable: true,
    width: '400px',
    footerButtons: [
        {text: 'Close', type: 'primary', handler () {
            priceModal.close();
        }}
    ]
});

document.addEventListener('click', event => {
    event.preventDefault();
    const btnType = event.target.dataset.btn;
    const id = +event.target.dataset.id;
    const fruit = fruits.find(f => f.id === id);

    if (btnType === 'price') {
        priceModal.setContent(`
            <p>Price for the ${fruit.title}: <strong>${fruit.price}$</strong></p>
        `)
        priceModal.open();
        console.log(fruit);
    } else if (btnType === 'remove') {
        $.confirm({
            title: 'Are you sure?',
            content: `<p>Are you sure for remove <strong>${fruit.title}?</strong></p>`
        }).then( () => {
            fruits = fruits.filter( f => f.id !== id);
            render();
        }).catch( () => {

        });
    }
})